import random
import os
import logging
import threading
from telegram import Update, ChatAction
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import openai

# Configurar los tokens
TOKEN = '6096295823:AAG167GUv2cx9pRz-_cJaeyEE0mYWfkamp0'
OPENAI_TOKEN = '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'

# Configurar el modelo de GPT
MODEL = "gpt-3.5-turbo-16k-0613"

# Configurar el logger
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

# Lista de personalidades y roles disponibles
personalities = ["amistoso", "serio", "divertido"]
roles = ["programador", "estudiante", "entusiasta de la tecnología"]

# Diccionario para almacenar la conversación de cada chat
conversations = {}

# Configurar la API de OpenAI
openai.api_key = OPENAI_TOKEN

# Función para manejar el comando /start
def start(update: Update, context):
    chat_id = update.effective_chat.id
    personality = random.choice(personalities)
    role = random.choice(roles)
    context.bot.send_message(chat_id=chat_id, text=f"Hola, soy un bot {personality} y estoy aquí para ayudarte. Soy un {role}. ¿En qué puedo ayudarte?")

# Resto del código...

# Obtener la clave API de la variable de entorno
OPENAI_TOKEN = os.environ.get("OPENAI_API_KEY")

# Verificar si la clave API está disponible
if OPENAI_TOKEN is None:
    logger.error("No se ha configurado la clave API de OpenAI.")
    exit()

# Funciones para manejar los mensajes del usuario y generar respuestas
# ...

def main():
    # Crear un objeto Updater y configurar el token del bot
    updater = Updater(token=TOKEN, use_context=True)

    # Obtener el despachador para registrar los controladores
    dispatcher = updater.dispatcher

    # Registrar el controlador para el comando /start
    start_handler = CommandHandler('start', start)
    dispatcher.add_handler(start_handler)

    # Registrar el controlador para los mensajes del usuario
    reply_handler = MessageHandler(Filters.text & (~Filters.command), reply_message)
    dispatcher.add_handler(reply_handler)

    # Iniciar el bot
    updater.start_polling()
    logger.info("Bot started")

    # Mantener el bot en ejecución hasta que se presione Ctrl+C
    updater.idle()

if __name__ == '__main__':
    main()
